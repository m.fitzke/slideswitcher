#!/bin/bash
name="SlideSwitcher"
strip=""
#flags=""
if [[ -z "$1" ]]
    then
        name="SlideSwitcher"
    else
        name=$1
fi

if [[ ! -z "$2" ]]
    then
        if [ $2 = "strip" ]
            then
                strip='-ldflags=-s'
                #flags="-s"
                printf "stripping all binaries...\n"
        fi
fi

if [ ! -d "build" ]; then
  mkdir build
fi


printf "building binaries..."
~/go/bin/fyne-cross linux,windows .


rm -rf build/fyne-cross
mv fyne-cross build/
#printf "building linux amd64 binary..."
#GOARCH=amd64 go build -o $name $strip ..
#printf "done\n"

#printf "builiding windows amd64 binary..."
#GOARCH=amd64 GOOS=windows go build -o $name".exe" $strip ..
#printf "done\n"
