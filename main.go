package main

import (
	"SlideSwitcher/errorhandler"
	"SlideSwitcher/fynegui"
	"SlideSwitcher/switcher"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

//VERSION const
const VERSION = "0.4"

var server bool = false
var debug bool = false
var port string = "4000"
var testdata = false

func main() {
	checkForArgs()
	configEnv()

	if server {
		switcher.StartServer(&port, nil)
	} else {
		fynegui.VERSION = VERSION
		fynegui.Run()
	}

}

func configEnv() {
	if !debug {
		gin.SetMode(gin.ReleaseMode)
	}

	if testdata {
		switcher.SwitcherClients = append(switcher.SwitcherClients, switcher.SwitcherClient{Name: "Test", IP: "localhost"})
		switcher.SwitcherClients = append(switcher.SwitcherClients, switcher.SwitcherClient{Name: "Test2", IP: "localhost2"})
		switcher.SwitcherClients = append(switcher.SwitcherClients, switcher.SwitcherClient{Name: "Test3", IP: "localhost3"})
		switcher.SwitcherClients = append(switcher.SwitcherClients, switcher.SwitcherClient{Name: "Test4", IP: "localhost4"})
		switcher.SwitcherClients = append(switcher.SwitcherClients, switcher.SwitcherClient{Name: "Test5", IP: "localhost5"})
	}

}

func checkForArgs() {
	args := os.Args
	for index, arg := range args {
		if arg == "-s" || arg == "-server" {
			server = true
			switcher.UseUpnp = true
		}

		if arg == "-cs" || arg == "-controlledserver" {
			server = true
		}

		if arg == "-p" || arg == "-port" {
			port = args[index+1]
		}

		if arg == "-d" || arg == "-debug" {
			errorhandler.Debug = true
			debug = true
		}

		if arg == "-t" || arg == "-test" {
			testdata = true
		}

		if arg == "-h" || arg == "-help" {
			log.Println("===============================================================")
			log.Println("Possible arguments for SliderSwitcher:")
			log.Println("")
			log.Println("-s or -server start server mode with UPNP")
			log.Println("-cs or -controlledserver start server mode without UPNP")
			log.Println("-p or -port to run on custom port (default is 4000")
			log.Println("-d or -debug enable debug output")
			log.Println("")
			log.Println("===============================================================")
			os.Exit(0)
		}
	}
}
