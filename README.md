# SlideSwitcher


## Client
Double click and connect to server. Use the left and right buttons in the app or the arrows on your keyboard to switch the slides.

Note: Default port 4000 can be omitted. If port is differen add `:port` behind ip.

## Server
Click `Start` to start the server and `Stop` to stop the server. If UPNP is checked the server tries to open a port via UPnP, otherwise the port has to be opened manually in the router.

### Headless/CLI only mode
Open a terminal/powershell and start the application with the `-s` flag.

## CLI Flags
* `-s` or `-server` : Start Server with websocket and UPNP
* `-cs` or `-controlledServer` : Star Server with websocket without UPNP
* `-p X` or `-port X` : Change default server port to `X`
* `-d` or `-debug` : Enable debugging output
* `-h` or `-help` : Print list of possible flags
