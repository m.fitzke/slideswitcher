import sys
import os 
import pyxhook 
import websocket
import time
import threading  
import signal

ip = ""
websock = None
clientName = "Python fallback client"

def OnKeyPress(event): 
    keystring = str(event.Key)
    if "Left" in keystring:
        SendToSocket("left")
    elif "Right" in keystring:
        SendToSocket("right")

def handleInterrupt(sig,frame):
    global websock
    if websock is not None:
        websock.close()
        print("closed connection")
    else:
        print("not connected")
    exit(0)

def connect():
    global websock
    global ip
    from websocket import create_connection
    if websock is None or not websock.connected:
        print("connecting...")
        websock = create_connection("ws://"+ip+"/switch")


def SendToSocket(msg):
    global websock
    if websock is None or not websock.connected:
        connect() 
    websock.send(clientName+";"+msg)
    print("Sent to server: " + msg)
    resp = websock.recv()
    print("Response from server: " + resp)
    
def KeepAlive():
    global websock
    while websock is not None and websock.connected:
        SendToSocket("keep-alive")
        time.sleep(5)

if len(sys.argv) == 2:
    ip = sys.argv[1]
    if ":" not in ip:
        ip += ":4000"
    print("Connecting to IP: " + ip)
else:
    print("No ip address given as parameter")
    sys.exit(1)
  
new_hook = pyxhook.HookManager() 
new_hook.KeyDown = OnKeyPress 
new_hook.HookKeyboard() 

try: 
    new_hook.start()
except KeyboardInterrupt:  
    pass
except Exception as ex: 

    print("error: " + str(ex))


connect()
x = threading.Thread(target=KeepAlive)
x.start()

signal.signal(signal.SIGINT,handleInterrupt)
signal.signal(signal.SIGTERM,handleInterrupt)