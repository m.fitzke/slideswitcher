import sys
import os 
import websocket
import time
import threading  
import signal
from tkinter import *
from tkinter import ttk
from websocket import create_connection

ip = ""
websock = None
clientName = "Python fallback client"

root = Tk()
root.title("SlideSwitcher Fallback Client")
frm = ttk.Frame(root, padding=10)

frm.grid()

threadVar = None
killThead = False

ipVar = StringVar()
connected = StringVar()
connected.set("disconnected")


def SendLeft():
    print("sending left")
    SendToSocket("left")

def SendRight():
    print("sending right")
    SendToSocket("right")

def handleInterrupt(sig,frame):
    global websock
    if websock is not None:
        websock.close()
        print("closed connection")
    else:
        print("not connected")
    exit(0)

def connect():
    global websock
    global ip
    global ipVar
    global connected
    
    if websock is None or not websock.connected:
        print("connecting...")
        ip = ipVar.get()
        if ip == "":
            print("no ip given")
            return False

        try:
            websock = create_connection("ws://"+ip+"/switch")
            if websock.connected:
                connected.set("connected")
                return True
        except:
            print("error connecting")
            connected.set("disconnected")
        
        return False


def SendToSocket(msg):
    global websock
    global connected
    if websock is None or not websock.connected:
        print("no connection, trying to connect to websocket")
        connect() 
    
    if websock is not None and websock.connected:
        try:
            websock.send(clientName+";"+msg)
            print("Sent to server: " + msg)
            resp = websock.recv()
            print("Response from server: " + resp)
        except:
            connected.set("disconnected")
            print("cannot send message, not connected to websocket")

    else:
        connected.set("disconnected")
        print("cannot send message, not connected to websocket")
    
def KeepAlive():
    global websock
    global killThead
    while websock is not None and websock.connected and not killThead:
        SendToSocket("keep-alive")
        time.sleep(5)
"""
if len(sys.argv) == 2:
    ip = sys.argv[1]
    if ":" not in ip:
        ip += ":4000"
    print("Connecting to IP: " + ip)
else:
    print("No ip address given as parameter")
    sys.exit(1)
"""


def connectToSocket():
    succ = connect()
    if succ:
        x = threading.Thread(target=KeepAlive)
        x.start()
        global threadVar
        threadVar = x
    else:
        print("not connected, no thread started")

def disc():
    global threadVar
    if threadVar is not None:
        print("disconnecting...")
        global killThead
        killThead = True
        threadVar.join()
        threadVar = None

        global websock
        websock.close()
        websock.shutdown()

        global connected
        connected.set("disconnected")


    else:
        print("not connected, cannot disconnect")



signal.signal(signal.SIGINT,handleInterrupt)
signal.signal(signal.SIGTERM,handleInterrupt)


ttk.Label(frm, text="IP:Port").grid(column=0, row=0)
ipEntry = ttk.Entry(frm, textvariable=ipVar).grid(column=1,row=0)
ttk.Button(frm, text="Connect", command=connectToSocket).grid(column=2,row=0)
ttk.Button(frm, text="Disconnect", command=disc).grid(column=3,row=0)
ttk.Button(frm, text="<", command=SendLeft).grid(column=0,row=3)
ttk.Button(frm, text=">", command=SendRight).grid(column=1,row=3)
#ttk.Button(frm, text="Quit", command=root.destroy).grid(column=3, row=3)
ttk.Label(frm,textvariable=connected).grid(column=3,row=3)

root.mainloop()

