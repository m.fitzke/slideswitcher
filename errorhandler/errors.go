package errorhandler

import "log"

//Debug var
var Debug = false

//LogError -> Generic error func
func LogError(err error, point string) {
	if err != nil {
		log.Println("Error point:", point)
		if Debug {
			log.Println(err.Error())
		}
	}

}
