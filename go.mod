module SlideSwitcher

go 1.15

require (
	fyne.io/fyne/v2 v2.0.0
	github.com/emersion/go-upnp-igd v0.0.0-20200714130311-4320956fc37f
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-vgo/robotgo v0.100.10
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	gopkg.in/olahol/melody.v1 v1.0.0-20170518105555-d52139073376
)
