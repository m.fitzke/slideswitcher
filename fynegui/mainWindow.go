package fynegui

import (
	"SlideSwitcher/switcher"
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
)

//VERSION string
var VERSION string

var switcherGui fyne.App = nil
var kill func()

//Run func
func Run() {
	initApp()
	switcherGui = app.New()
	mainWindow := switcherGui.NewWindow("Slide Switcher")
	mainWindow.Resize(fyne.NewSize(500, 200))
	mainWindow.SetContent(buildMainContent())

	mainWindow.Canvas().SetOnTypedKey(func(k *fyne.KeyEvent) {
		if k.Name == fyne.KeyRight {
			log.Println("tapped right on keyboard")
			switcher.SendMessage("right")
		} else if k.Name == fyne.KeyLeft {
			log.Println("tapped left on keyboard")
			switcher.SendMessage("left")
		}
	})

	mainWindow.ShowAndRun()

}

//initApp function for GUI
func initApp() bool {
	if switcherGui == nil {

		switcherGui = app.New()
		return true
	}
	return false
}

func buildMainContent() *container.AppTabs {

	clientContainer := buildClientContainer()
	serverContainer := buildServerContainer()
	tabs := container.NewAppTabs(
		container.NewTabItem("Client", clientContainer),
		container.NewTabItem("Server", serverContainer),
	)

	tabs.SetTabLocation(container.TabLocationTop)
	return tabs
}
