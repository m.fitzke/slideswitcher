package fynegui

import (
	"SlideSwitcher/errorhandler"
	"SlideSwitcher/switcher"
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

//var clientListRefresh func()
var clientListRef **widget.Form
var clientListContainerRef **fyne.Container
var exchangeTabContainer func(index int, tab *container.TabItem)

func buildServerContainer() *container.AppTabs {
	serverControlContainer := buildServerControlContainer()
	clientListContainer := buildServerClientListContainer()

	tabs := container.NewAppTabs(

		container.NewTabItem("Control", serverControlContainer),
		container.NewTabItem("Clients", clientListContainer),
	)

	exchangeTabContainer = func(index int, tab *container.TabItem) {
		tabs.RemoveIndex(index)
		tabs.Append(tab)
		tabs.SelectTabIndex(index)
	}

	tabs.SetTabLocation(container.TabLocationLeading)
	return tabs

}

func addNewClientsToListView() {
	log.Println("Refreshing client list...")
	log.Println(switcher.SwitcherClients)
	if clientListRef != nil {
		list := (*clientListRef)
		for i := 0; i < len(switcher.SwitcherClients); i++ {
			cl := &switcher.SwitcherClients[i]
			it := buildClientListItem(cl)

			exists := false
			for j := 0; j < len(list.Items); j++ {
				if list.Items[j].Text == cl.Name+" | "+cl.IP {
					exists = true
					list.Items[j] = it
					break
				}
			}
			if !exists {
				list.AppendItem(it)
			}

		}
		//list.Refresh()
	}
}

func refreshClientTab() {
	for i := 0; i < len(switcher.SwitcherClients); i++ {
		if !switcher.SwitcherClients[i].Connected() {
			switcher.SwitcherClients = append(switcher.SwitcherClients[:i], switcher.SwitcherClients[i+1:]...)
			i--
		}
	}

	clientListContainer := buildServerClientListContainer()
	tabItem := container.NewTabItem("Clients", clientListContainer)

	if exchangeTabContainer != nil {
		exchangeTabContainer(1, tabItem)
	}
}

func removeClientFromList(cl switcher.SwitcherClient) {
	cl.Disconnect()
	switcher.RemoveClient(cl)
	refreshClientTab()

	/*
		clientList := widget.NewForm()
		*clientListRef = clientList

		clientListContainer := container.New(layout.NewMaxLayout(), clientList)
		*clientListContainerRef = clientListContainer

		addNewClientsToListView()
	*/

	/*if clientListRef != nil {
	list := (*clientListRef)

	//exists := false
	for j := 0; j < len(list.Items); j++ {
		if list.Items[j].Text == cl.Name+" | "+cl.IP {
			//exists = true
			list.Items = append(list.Items[:j], list.Items[j+1:]...)

			//list.Items[j] = it
			break
		}
	}
	/*if !exists {
		list.AppendItem(it)
	}*/

	//list.Refresh()

	//}

	/*list := (*clientListRef)

	log.Println("List Size before:", len(list.Items))
	for j := 0; j < len(list.Items); j++ {
		if list.Items[j].Text == cl.Name+" | "+cl.IP {
			list.Items = append(list.Items[:j], list.Items[j+1:]...)
			break
		}
	}
	list.Refresh()
	log.Println("List Size after:", len(list.Items))
	clientList := widget.NewForm()
	clientListRef = &clientList
	*/
	//addNewClientsToListView()
}

func buildClientListItem(cl *switcher.SwitcherClient) *widget.FormItem {

	permissionButtonLabel := "Allow"
	if cl.Permission {
		permissionButtonLabel = "Block"
	}

	var changeLabel func()

	permissionButton := widget.NewButton(permissionButtonLabel, func() {
		cl.Permission = !cl.Permission
		addNewClientsToListView()
		if changeLabel != nil {
			changeLabel()
		}

	})

	banButton := widget.NewButton("Ban", func() {
		removeClientFromList(*cl)
	})

	changeLabel = func() {
		if cl.Permission {
			permissionButton.Text = "Block"
		} else {
			permissionButton.Text = "Allow"
		}
		permissionButton.Refresh()
	}

	buttonContainer := container.NewHBox(layout.NewSpacer(), permissionButton, banButton)
	fi := widget.NewFormItem(cl.Name+" | "+cl.IP, buttonContainer)

	return fi
}

func buildServerClientListContainer() *fyne.Container {

	refreshButton := widget.NewButton("Refresh", func() {
		//addNewClientsToListView()
		refreshClientTab()
	})

	clientList := widget.NewForm()
	clientListRef = &clientList

	clientListContainer := container.New(layout.NewMaxLayout(), clientList)
	clientListContainerRef = &clientListContainer

	addNewClientsToListView()

	listScrollContainer := container.NewScroll(clientListContainer)
	fullContainer := container.New(layout.NewBorderLayout(refreshButton, nil, nil, nil), refreshButton, listScrollContainer)
	return fullContainer
}

func buildServerControlContainer() *fyne.Container {

	portInput := widget.NewEntry()
	portInput.SetPlaceHolder("Port (default is 4000)")

	upnpCheckbox := widget.NewCheck("UPnP", func(b bool) {
		switcher.UseUpnp = b
	})
	upnpCheckbox.SetChecked(true)

	allowAllClientsCheckbox := widget.NewCheck("All Clients", func(b bool) {
		switcher.AllowAllClients = b
	})
	allowAllClientsCheckbox.SetChecked(false)

	var startButton *widget.Button
	var stopButton *widget.Button

	startButton = widget.NewButton("Start Server", func() {

		port := "4000"

		if portInput.Text != "" {
			port = portInput.Text
		}
		switcher.Disconnect()

		go switcher.StartServer(&port, &kill)

	})

	stopButton = widget.NewButton("Stop Server", func() {

		if kill != nil {
			kill()
		}

	})

	b := binding.BindString(&switcher.ServerState)
	serverStateLabel := widget.NewLabelWithData(b)
	switcher.AddServerCallback(func() {
		log.Println("Reloading")
		err := b.Reload()
		if err != nil {
			//handleError
			errorhandler.LogError(err, "Server callback for label change")
		}
	})

	versionLabel := widget.NewLabel("v" + VERSION)

	exitButton := widget.NewButton("Exit", func() {
		switcher.Disconnect()
		switcherGui.Quit()
	})

	settingsBar := container.New(layout.NewVBoxLayout(), layout.NewSpacer(), portInput, layout.NewSpacer())
	controlBar := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), startButton, stopButton, layout.NewSpacer())
	buttonBar := container.New(layout.NewHBoxLayout(), versionLabel, layout.NewSpacer(), serverStateLabel, layout.NewSpacer(), exitButton)
	centerContent := container.New(layout.NewVBoxLayout(), settingsBar, controlBar, container.NewHBox(layout.NewSpacer(), upnpCheckbox, layout.NewSpacer(), allowAllClientsCheckbox, layout.NewSpacer()))
	serverControlContainer := container.New(layout.NewBorderLayout(nil, buttonBar, nil, nil), buttonBar, centerContent)

	return serverControlContainer
}
