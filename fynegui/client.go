package fynegui

import (
	"SlideSwitcher/errorhandler"
	"SlideSwitcher/switcher"
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func buildClientContainer() *fyne.Container {
	rightButton := widget.NewButton(">", func() {
		switcher.SendMessage("right")
	})

	leftButton := widget.NewButton("<", func() {
		switcher.SendMessage("left")
	})

	versionLabel := widget.NewLabel("v" + VERSION)

	exitButton := widget.NewButton("Exit", func() {
		switcher.Disconnect()
		switcherGui.Quit()
	})

	nameInput := widget.NewEntry()
	nameInput.SetPlaceHolder("Your name")

	connectInput := widget.NewEntry()
	connectInput.SetPlaceHolder("IP:Port (default is localhost:4000)")

	b := binding.BindString(&switcher.ConnectedString)
	connectedLabel := widget.NewLabelWithData(b)
	switcher.AddClientCallback(func() {
		log.Println("Reloading")
		err := b.Reload()
		if err != nil {
			//handleError
			errorhandler.LogError(err, "Client callback for label change")
		}
	})

	connectButton := widget.NewButton("Connect", func() {
		if kill != nil {
			kill()
		}

		if switcher.Connect(nameInput.Text, connectInput.Text) {
			connectedLabel.SetText("connected")
		}

	})

	disconnectButton := widget.NewButton("Disconnect", func() {
		if switcher.Disconnect() {
			connectedLabel.SetText("State: disconnected")
		}
	})

	/*
		connectionBar := fyne.NewContainerWithLayout(layout.NewVBoxLayout(), nameInput, connectInput, container.NewHBox(layout.NewSpacer(), connectButton, disconnectButton , layout.NewSpacer()))
		buttonBar := fyne.NewContainerWithLayout(layout.NewHBoxLayout(), versionLabel, layout.NewSpacer(), connectedLabel, layout.NewSpacer(), exitButton)
		switchers := fyne.NewContainerWithLayout(layout.NewHBoxLayout(), layout.NewSpacer(), leftButton, rightButton, layout.NewSpacer())
		clientContainer := fyne.NewContainerWithLayout(layout.NewBorderLayout(connectionBar, buttonBar, nil, nil), switchers, connectionBar, buttonBar)
	*/

	connectionBar := container.New(layout.NewVBoxLayout(), nameInput, connectInput, container.NewHBox(layout.NewSpacer(), connectButton, disconnectButton, layout.NewSpacer()))
	buttonBar := container.New(layout.NewHBoxLayout(), versionLabel, layout.NewSpacer(), connectedLabel, layout.NewSpacer(), exitButton)
	switchers := container.New(layout.NewVBoxLayout(), container.NewHBox(layout.NewSpacer(), leftButton, rightButton, layout.NewSpacer()))
	clientContainer := container.New(layout.NewBorderLayout(connectionBar, buttonBar, nil, nil), switchers, connectionBar, buttonBar)

	return clientContainer
}
