package switcher

import (
	"log"
	"strings"
)

func executeCommand(client SwitcherClient, cmd string) string {

	if cmd == "keep-alive" {
		return "received keep-alive from " + client.Name
	}

	if cmd == "disconnect" {
		Disconnect()
		return "connection was terminated by server"
	}

	log.Println("Client for command:", client)

	if !client.Permission && !AllowAllClients {
		return client.Name + " does not have persmissions"
	}

	resp := ""
	if cmd == "right" {
		Right()
		resp = "received right from " + client.Name
	} else if cmd == "left" {
		Left()
		resp = "received left from " + client.Name
	} else {
		resp = "wrong message received from " + client.Name
	}
	return resp
}

func parseMessage(msg string) (string, string) {
	msgSplit := strings.Split(msg, ";")
	senderName := "unknown client"
	var cmd = msg
	if len(msgSplit) > 1 {
		senderName = msgSplit[0]
		cmd = msgSplit[1]
	}

	return cmd, senderName
}
