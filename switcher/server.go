package switcher

import (
	"SlideSwitcher/errorhandler"
	"context"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	igd "github.com/emersion/go-upnp-igd"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gopkg.in/olahol/melody.v1"
)

//UseUpnp var
var UseUpnp = false

var upnpPort int = 0

var igdClient igd.Device
var ServerState = "State: offline"
var AllowAllClients bool = false
var serverCallback func()

//StartServer func
func StartServer(port *string, kill *func()) {
	if UseUpnp {
		go openUpnp(*port)
	}

	startAPI(port, kill)
}

func AddServerCallback(c func()) {
	serverCallback = c
}

func openUpnp(port string) {
	log.Println("Trying to open port via UPNP")
	devices := make(chan igd.Device)
	go func() {
		for d := range devices {
			//log.Println(d)
			ip, devErr := d.GetExternalIPAddress()
			if devErr != nil {
				errorhandler.LogError(devErr, "upnp getting external address")
				continue
			}

			log.Println("External ip: " + ip.String() + ":" + port)

			var portInt int
			if portInt, devErr = strconv.Atoi(port); devErr != nil {
				errorhandler.LogError(devErr, "upnp parsing port")
				continue
			}
			upnpPort = portInt
			seed := strconv.FormatInt(rand.Int63(), 10)
			_, devErr = d.AddPortMapping(igd.TCP, upnpPort, upnpPort, "Slide Switcher Server "+seed, 60*time.Minute)
			if devErr != nil {
				errorhandler.LogError(devErr, "upnp opening port")
				continue
			}

			igdClient = d
			log.Println("UPNP port open")
			break
		}
	}()
	err := igd.Discover(devices, 15*time.Second)
	if err != nil {
		errorhandler.LogError(err, "upnp device discovery")
		return
	}

}

func closeUpnpPort() {

}

func startAPI(port *string, kill *func()) {

	var err error
	api := gin.Default()
	socket := melody.New()
	api.Use(cors.Default())

	api.GET("/", func(c *gin.Context) {
		_, err = c.Writer.WriteString("Slide Switcher Server")
		errorhandler.LogError(err, "/")
	})

	api.GET("/switch", func(c *gin.Context) {
		if ClientIsBanned(c.Request.RemoteAddr) {
			log.Println("stopping banned client from connecting...")
			return
		}
		socket.HandleRequest(c.Writer, c.Request)
	})

	socket.HandleMessage(func(s *melody.Session, msg []byte) {
		messageString := string(msg)
		log.Println("message received:", messageString)

		cmd, name := parseMessage(messageString)

		ip := strings.Split(s.Request.RemoteAddr, ":")[0]

		currentClient := SwitcherClient{Name: name, IP: ip, Socket: s}
		log.Println("Name:", currentClient.Name, "; IP:", currentClient.IP)
		if ok, cl := ClientExists(currentClient); !ok {
			log.Println("Client is new!")
			SwitcherClients = append(SwitcherClients, currentClient)
		} else {
			log.Println("Client is known!")
			currentClient = cl
		}

		resp := executeCommand(currentClient, cmd)

		log.Println(resp)
		err = s.Write([]byte(resp))
		if err != nil {
			errorhandler.LogError(err, "socket response error")
			s.Write([]byte("error while receiving message from socket"))
		}

		//return
	})

	srv := &http.Server{
		Addr:    ":" + *port,
		Handler: api,
	}

	go func() {
		defer closeUpnpPort()
		ServerState = "State: online"
		serverCallback()
		if err := srv.ListenAndServe(); err != nil {
			errorhandler.LogError(err, "error while running server")
			ServerState = "State: offline"
			serverCallback()
			/*defer func() {
				if r := recover(); r != nil {
					log.Println("recovered from:", err.Error())
				}
			}()*/
		}

	}()

	if kill != nil {
		*kill = func() {
			ctx, cancel := context.WithCancel(context.Background())
			ServerState = "State: offline"
			serverCallback()
			log.Println("Server stopped")
			defer cancel()

			err = srv.Shutdown(ctx)
			if err != nil {
				defer func() {
					if r := recover(); r != nil {
						log.Println("recovered from:", err.Error())
					}
				}()
			}

			for i := 0; i < len(SwitcherClients); i++ {
				SwitcherClients[i].Disconnect()
			}

			for i := 0; i < len(DeletedSwitcherClients); i++ {
				DeletedSwitcherClients[i].Disconnect()
			}

			err = socket.Close()
			if err != nil {
				defer func() {
					if r := recover(); r != nil {
						log.Println("recovered from:", err.Error())
					}
				}()
			}

		}
	}

}
