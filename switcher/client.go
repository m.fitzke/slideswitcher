package switcher

import (
	"SlideSwitcher/errorhandler"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"golang.org/x/net/websocket"
)

var socket *websocket.Conn = nil

//Kill var
var kill bool = false
var wg sync.WaitGroup
var connected bool = false
var clientCallback func()
var clientName string = "unknown client"

//ConnectedString var
var ConnectedString = "State: disconnected"

//SyncWithOthers var
var SyncWithOthers bool = false

//AddClientCallback func
func AddClientCallback(c func()) {
	clientCallback = c
}

//Connect func
func Connect(name, ip string) bool {
	if name != "" {
		clientName = name
	}

	if !strings.Contains(ip, ":") {
		ip += ":4000"
	}
	var err error
	socket, err = websocket.Dial("ws://"+ip+"/switch", "", "http://localhost")
	if err != nil {
		errorhandler.LogError(err, "websocket connection")
		return false
	}
	log.Println("connected to " + ip)
	connected = true
	ConnectedString = "connected"
	kill = false

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	wg.Add(1)
	go func() {

		for !kill && connected {
			var newMsg = make([]byte, 512)
			var n int
			if n, err = socket.Read(newMsg); err != nil {
				errorhandler.LogError(err, "receiving message error")
				connected = false
				ConnectedString = "State: disconnected"
				kill = true
				clientCallback()
				break
			}
			log.Println("Response from server: " + string(newMsg[:n]))
			//if n > 0 {
			//evaluateMessage(string(newMsg[:n]))
			//}
		}
		wg.Done()

	}()
	wg.Add(1)
	go func() {

		for !kill && connected {
			SendMessage("keep-alive")
			time.Sleep(5 * time.Second)
		}
		wg.Done()

	}()

	go func() {
		<-sigs
		kill = true
		wg.Wait()
	}()

	return true
}

//Disconnect func
func Disconnect() bool {
	if socket != nil {
		ConnectedString = "State: disconnected"
		socket.Close()
		socket = nil
		return true
	}
	return false
}

//SendMessage func
func SendMessage(msg string) bool {
	msg = clientName + ";" + msg
	if socket != nil {
		_, err := socket.Write([]byte(msg))
		if err != nil {
			errorhandler.LogError(err, "sending message error")
			connected = false
			ConnectedString = "State: disconnected"
			kill = true
			clientCallback()
			return false
		}
		log.Println("Message to server: " + msg)

		return true
	}
	log.Println("not connected")
	return false
}
