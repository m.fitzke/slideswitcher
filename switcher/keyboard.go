//go:build !android
// +build !android

package switcher

import "github.com/go-vgo/robotgo"

//Left func
func Left() {
	robotgo.KeyTap("left")
}

//Right func
func Right() {
	robotgo.KeyTap("right")
}
