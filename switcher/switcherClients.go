package switcher

import (
	"log"
	"strings"

	"gopkg.in/olahol/melody.v1"
)

var SwitcherClients []SwitcherClient
var DeletedSwitcherClients []SwitcherClient

type SwitcherClient struct {
	Name       string
	IP         string
	Socket     *melody.Session
	Permission bool
}

func ClientExists(sc SwitcherClient) (bool, SwitcherClient) {
	for i := 0; i < len(SwitcherClients); i++ {
		if sc.IP == SwitcherClients[i].IP && sc.Name == SwitcherClients[i].Name {
			return true, SwitcherClients[i]
		}
	}
	return false, SwitcherClient{}
}

func RemoveClient(sc SwitcherClient) {
	DeletedSwitcherClients = append(DeletedSwitcherClients, sc)
	for i := 0; i < len(SwitcherClients); i++ {
		if sc == SwitcherClients[i] {
			SwitcherClients = append(SwitcherClients[:i], SwitcherClients[i+1:]...)
		}
	}
}

func ClientIsBanned(ip string) bool {
	ip = strings.Split(ip, ":")[0]
	for i := 0; i < len(DeletedSwitcherClients); i++ {
		if ip == DeletedSwitcherClients[i].IP {
			return true
		}
	}
	return false
}

func (sc *SwitcherClient) Allow() {
	sc.Permission = true
}

func (sc *SwitcherClient) Block() {
	sc.Permission = false
}

func (sc *SwitcherClient) Connected() bool {
	return !sc.Socket.IsClosed()
}

func (sc *SwitcherClient) Disconnect() {
	log.Println("Disconnect attempt...")
	if sc.Socket != nil {
		//sc.Socket.CloseWithMsg([]byte("Connection was terminated by Server"))
		sc.Socket.Close()

	}

}
